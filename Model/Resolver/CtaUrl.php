<?php


namespace Givex\Ctalink\Model\Resolver;


use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Quote\Model\MaskedQuoteIdToQuoteId;
use Givex\Ctalink\Helper\Data;

class CtaUrl implements ResolverInterface
{
    /**
     * @var Cart
     */
    private $dataHelper;
    /**
     * @var CartRepositoryInterface
     */
    private $cartRepository;
    /**
     * @var MaskedQuoteIdToQuoteId
     */
    private $maskedQuoteIdToQuoteId;

    /**
     * RemoveGiftWrap constructor.
     * @param Cart $dataHelper
     * @param CartRepositoryInterface $cartRepository
     * @param MaskedQuoteIdToQuoteId $maskedQuoteIdToQuoteId
     */
    public function __construct(
        Data $dataHelper,
        CartRepositoryInterface $cartRepository,
        MaskedQuoteIdToQuoteId $maskedQuoteIdToQuoteId,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ){
        $this->dataHelper = $dataHelper;
        $this->cartRepository = $cartRepository;
        $this->maskedQuoteIdToQuoteId = $maskedQuoteIdToQuoteId;
        $this->_scopeConfig = $scopeConfig;
    }
    /**
     * @inheritDoc
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        return ['givex_ctalink' => $this->dataHelper->getGeneralConfig('url')];
    }
}
